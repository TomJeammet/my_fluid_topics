import { Injectable } from "@angular/core";
import { FtSearchFilters } from "src/models/fluid-topics/ft-search-filters/ft-search-filters";
import { FluidTopicsQuery } from "src/store/fluid_topics/fluid-topics.query";

@Injectable({ providedIn: 'root' })
export class FluidTopicsService {

  documents$ = this.fluidTopicsQuery.documents$;
  maps$ = this.fluidTopicsQuery.maps$;
  map$ = this.fluidTopicsQuery.map$;
  mapToc$ = this.fluidTopicsQuery.mapToc$;
  searchResult$ = this.fluidTopicsQuery.searchResult$;
  suggestionResult$ = this.fluidTopicsQuery.suggestionResult$;
  locales$ = this.fluidTopicsQuery.locales$;
  topics$ = this.fluidTopicsQuery.topics$;

  constructor(
    private fluidTopicsQuery: FluidTopicsQuery
  ) { }

  fetchLocales() {
    this.fluidTopicsQuery.fetchLocales();
  }

  fetchDocuments() {
    this.fluidTopicsQuery.fetchDocuments()
  }

  getDocument(url: string) {
    return this.fluidTopicsQuery.getDocument(url);
  }

  fetchMaps() {
    this.fluidTopicsQuery.fetchMaps();
  }

  fetchMap(id: string) {
    this.fluidTopicsQuery.fetchMap(id);
  }

  getMap(id: string) {
    return this.fluidTopicsQuery.getMap(id);
  }

  fetchMapTableOfContent(id: string) {
    this.fluidTopicsQuery.fetchMapTableOfContent(id);
  }

  fetchMapTopics(id: string) {
    this.fluidTopicsQuery.fetchMapTopics(id);
  }

  fetchSearchResult(searchFilters: FtSearchFilters) {
    this.fluidTopicsQuery.fetchSearchResult(searchFilters);
  }

  getDocumentContent(id: string) {
    return this.fluidTopicsQuery.getDocumentContent(id);
  }

  fetchSuggest(query: string) {
    this.fluidTopicsQuery.fetchSuggest(query);
  }

  getTopicContent(mapId: string, contentId: string) {
    return this.fluidTopicsQuery.getTopicContent(mapId, contentId);
  }
}