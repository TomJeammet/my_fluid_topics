import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { FtContentLocales } from "src/models/fluid-topics/common/ft-content-locales";
import { FtDocument } from "src/models/fluid-topics/common/ft-document";
import { FtMap } from "src/models/fluid-topics/common/map/ft-map";
import { FtMapTableOfContentElement } from "src/models/fluid-topics/common/map/ft-map-table-of-content";
import { FtTopic } from "src/models/fluid-topics/common/topic/ft-topic";
import { FtSearchFilters } from "src/models/fluid-topics/ft-search-filters/ft-search-filters";
import { FtSearchResult } from "src/models/fluid-topics/ft-search-results/ft-search-result";
import { FtSuggestionsResult } from "src/models/fluid-topics/ft-suggestions/ft-suggestions-result";
import { FluidTopicsState } from "./fluid-topics.state";
import { FluidTopicsStore } from "./fluid-topics.store";

@Injectable({ providedIn: 'root' })
export class FluidTopicsQuery extends Query<FluidTopicsState> {

  private readonly baseUrl = '/api/khub';

  documents$ = this.select('documents');
  maps$ = this.select('maps');
  map$ = this.select('map');
  mapToc$ = this.select('mapToc');
  searchResult$ = this.select('searchResult');
  suggestionResult$ = this.select('suggestionResult');
  locales$ = this.select('locales');
  topics$ = this.select('topics');

  httpOptions = {
    headers: new HttpHeaders({ 
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    })
  };

  constructor(
    protected http: HttpClient,
    protected override store: FluidTopicsStore,
  ) { 
    super(store);
  }

  fetchLocales() {
    this.getLocales()
      .subscribe(contentLocales => this.store.update(_ => ({ locales: contentLocales.contentLocales })));
  }

  private getLocales() {
    return this.http.get<FtContentLocales>(this.baseUrl + '/locales', this.httpOptions);
  }

  fetchDocuments() {
    this.getDocuments()
      .subscribe(documents => this.store.update(_ => ({ documents })));
  }

  private getDocuments() {
    return this.http.get<FtDocument[]>(this.baseUrl + '/documents', this.httpOptions);
  }

  getDocument(url: string) {
    return this.http.get<FtDocument>(url, this.httpOptions);
  }

  fetchMaps() {
    this.getMaps()
      .subscribe(maps => this.store.update(_ => ({ maps })));
  }

  private getMaps() {
    return this.http.get<FtMap[]>(this.baseUrl + '/maps', this.httpOptions);
  }

  fetchMapTableOfContent(id: string) {
    this.getMapTableOfContent(id)
      .subscribe(mapToc => this.store.update(_ => ({ mapToc })));
  }

  getMapTableOfContent(id: string) {
    return this.http.get<FtMapTableOfContentElement[]>(this.baseUrl + '/maps/' + id + '/toc', this.httpOptions);
  }

  fetchMapTopics(id: string) {
    this.getMapTopics(id)
      .subscribe(topics => this.store.update(_ => ({ topics })));
  }

  getMapTopics(id: string) {
    return this.http.get<FtTopic[]>(this.baseUrl + '/maps/' + id + '/topics', this.httpOptions);
  }

  fetchMap(id: string) {
    this.getMap(id)
      .subscribe(map => this.store.update(_ => ({ map })));
  }

  getMap(id: string) {
    return this.http.get<FtMap>(this.baseUrl + '/maps/' + id, this.httpOptions);
  }

  fetchSearchResult(searchFilters: FtSearchFilters) {
    this.searchQuery(searchFilters)
      .subscribe(searchResult => this.store.update(_ => ({ searchResult })));
  }

  private searchQuery(searchFilters: FtSearchFilters): Observable<FtSearchResult> {
    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }),
    };
    return this.http.post<FtSearchResult>(this.baseUrl + '/clustered-search', searchFilters, options);
  }

  getDocumentContent(id: string) {

    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
      }),
      responseType: 'text' as const
    };

    return this.http.get(this.baseUrl + '/documents/' + id + '/content', options);
  }

  fetchSuggest(query: string) {
    this.suggest(query)
      .subscribe(suggestionResult => this.store.update(_ => ({ suggestionResult })));
  }

  private suggest(query: string): Observable<FtSuggestionsResult> {
    let params = new HttpParams({});
    params.append('query', query);
    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }),
    };
    return this.http.post<FtSuggestionsResult>(this.baseUrl + '/suggest', { "input": query }, options);
  }

  getTopicContent(mapId: string, contentId: string) {

    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
      }),
      responseType: 'text' as const
    };

    return this.http.get(this.baseUrl + '/maps/' + mapId + '/topics/' + contentId + '/content', options);
  }

}