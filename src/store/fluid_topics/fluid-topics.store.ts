import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { FluidTopicsState, createInitialFluidTopicsState } from './fluid-topics.state';

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'fluidTopics' })
export class FluidTopicsStore extends Store<FluidTopicsState> {
  constructor() {
    super(createInitialFluidTopicsState());
  }
}
