import { FtDocument } from "src/models/fluid-topics/common/ft-document";
import { FtLocale } from "src/models/fluid-topics/common/ft-locale";
import { FtMap } from "src/models/fluid-topics/common/map/ft-map";
import { FtMapTableOfContentElement } from "src/models/fluid-topics/common/map/ft-map-table-of-content";
import { FtTopic } from "src/models/fluid-topics/common/topic/ft-topic";
import { defaultSearchResult, FtSearchResult } from "src/models/fluid-topics/ft-search-results/ft-search-result";
import { defaultSuggestionResult, FtSuggestionsResult } from "src/models/fluid-topics/ft-suggestions/ft-suggestions-result";

export interface FluidTopicsState {
  documents: FtDocument[];
  maps: FtMap[];
  map?: FtMap;
  mapToc?: FtMapTableOfContentElement[];
  searchResult: FtSearchResult;
  suggestionResult: FtSuggestionsResult;
  locales: FtLocale[];
  topics: FtTopic[];
}

export function createInitialFluidTopicsState(): FluidTopicsState {
  return {
    documents: [],
    maps: [],
    map: undefined,
    mapToc: [],
    searchResult: defaultSearchResult,
    suggestionResult: defaultSuggestionResult,
    locales: [],
    topics: []
  };
}