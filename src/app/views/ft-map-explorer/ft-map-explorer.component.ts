import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzTreeNodeOptions } from 'ng-zorro-antd/core/tree';
import { map } from 'rxjs/operators';
import { untilDestroyed } from 'src/helpers/rxjs-utils';
import { FtMap } from 'src/models/fluid-topics/common/map/ft-map';
import { FtMapTableOfContentElement } from 'src/models/fluid-topics/common/map/ft-map-table-of-content';
import { FtTopic } from 'src/models/fluid-topics/common/topic/ft-topic';
import { FtTopicContent } from 'src/models/fluid-topics/common/topic/ft-topic-content';
import { FluidTopicsService } from 'src/services/fluid-topics-api-service';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { combineLatest, Subscription } from 'rxjs';

@Component({
  selector: 'ft-map-explorer',
  templateUrl: './ft-map-explorer.component.html',
  styleUrls: ['./ft-map-explorer.component.scss']
})
export class FtMapExplorerComponent implements OnInit, OnDestroy {

  @ViewChild('topicScroller') viewPort: CdkVirtualScrollViewport | undefined;

  maps: FtMap[] = [];
  mapTopics: FtTopic[] = [];
  mapTopicsContent: FtTopicContent[] = [];

  selectedMap?: FtMap;

  topicsLoaded: boolean = false;
  tocLoaded: boolean = false;

  routeParams$ = this.route.paramMap
  .pipe(
    map(paramMap => [
      paramMap.get('mapId'),
      paramMap.get('topicId')
    ])
  );

  mapSubscription: Subscription = new Subscription();
  topicsSubscription: Subscription = new Subscription();
  mapsSubscription: Subscription = new Subscription();
  mapTocSubscription: Subscription = new Subscription();

  topicId?: string;

  isAMapSelected: boolean = false;

  mapToc: FtMapTableOfContentElement[] = [];
  data: NzTreeNodeOptions[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fluidTopicsService: FluidTopicsService
  ) { }

  ngOnInit(): void {

    this.fluidTopicsService.maps$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(maps => {
      this.maps = maps;
    });

    this.fluidTopicsService.fetchMaps();

    this.fluidTopicsService.mapToc$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(mapToc => {
      if (mapToc !== undefined && mapToc.length > 0) {
        this.tocLoaded = true;
        this.mapToc = mapToc;
        const nodes = this.fillTreeData(mapToc, []);
        this.data = [ ...nodes ];
      }
    });

    this.fluidTopicsService.topics$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(topics => {
      this.mapTopicsContent = [];
      this.mapTopics = topics;
      if (topics && topics.length > 0 && this.selectedMap) {
        for (const topic of topics) {
          const topicHtmlTitle = this.determineTopicTitleLevel(topic);
          this.fluidTopicsService.getTopicContent(this.selectedMap?.id, topic.id).subscribe(content => {
            const topicContent: FtTopicContent = {
              id: topic.id,
              title: topicHtmlTitle,
              content
            };
            this.mapTopicsContent.push(topicContent);
            if (this.mapTopicsContent.length === topics.length) {
              this.topicsLoaded = true;
              setTimeout(() => {
                if (this.topicId) {
                  this.scrollToTopicId(this.topicId);
                }
              });
            }
          });
        }
      }
    });

    this.mapSubscription = this.fluidTopicsService.map$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(map => {
      if (map !== undefined && map.id) {
        this.topicsLoaded = false;
        this.tocLoaded = false;
        this.selectedMap = map;
        this.fluidTopicsService.fetchMapTableOfContent(map.id);
        this.fluidTopicsService.fetchMapTopics(map.id);
      }
    });

    combineLatest([
      this.routeParams$
    ])
    .subscribe(([ [ mapId, topicId ] ]) => {

      if (mapId !== null) {
        if (topicId) {
          this.topicId = topicId;
        }
        this.isAMapSelected = true;
        this.fluidTopicsService.fetchMap(mapId);
      } else {
        this.isAMapSelected = false;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.mapSubscription) {
      this.mapSubscription.unsubscribe();
    }
    if (this.topicsSubscription) {
      this.topicsSubscription.unsubscribe();
    }
    if (this.mapsSubscription) {
      this.mapsSubscription.unsubscribe();
    }
    if (this.mapTocSubscription) {
      this.mapTocSubscription.unsubscribe();
    }
  }

  fillTreeData(mapToc: FtMapTableOfContentElement[], nodes: NzTreeNodeOptions[], basicKey: number = 0) {
    for (const element of mapToc) {
      const node: NzTreeNodeOptions = {
        title: element.title,
        key: basicKey.toString(),
        expanded: true,
        children: this.fillTreeData(element.children, [], basicKey * 10),
        tocId: element.tocId,
        contentId: element.contentId
      };
      nodes.push(node);
      basicKey++;
    }
    return nodes;
  }
  
  determineTopicTitleLevel(topic: FtTopic) {
    let topicHtmlTitle: string;
    if (topic.breadcrumb && topic.breadcrumb.length > 0) {
      switch (topic.breadcrumb.length) {
        case 1:
          topicHtmlTitle = '<h1>'+ topic.title + '</h1>';
          break;
        case 2:
          topicHtmlTitle = '<h2>'+ topic.title + '</h2>';
          break;
        case 3:
          topicHtmlTitle = '<h3>'+ topic.title + '</h3>';
          break;
        case 4:
          topicHtmlTitle = '<h4>'+ topic.title + '</h4>';
          break;
        case 5:
          topicHtmlTitle = '<h5>'+ topic.title + '</h5>';
          break;
        case 6:
          topicHtmlTitle = '<h6>'+ topic.title + '</h6>';
          break;
        default:
          topicHtmlTitle = '<h6>'+ topic.title + '</h6>';
          break;
      }
    } else {
      topicHtmlTitle = '<h6>'+ topic.title + '</h6>';
    }
    return topicHtmlTitle;
  }

  openMap(mapId: string) {
    this.router.navigateByUrl('/maps/' + mapId);
  }

  goBackToMapList() {
    this.selectedMap = undefined;
    this.mapToc = [];
    this.topicId = undefined;
    this.data = [];
    if (this.mapSubscription) {
      this.mapSubscription.unsubscribe();
    }
    if (this.topicsSubscription) {
      this.topicsSubscription.unsubscribe();
    }
    if (this.mapsSubscription) {
      this.mapsSubscription.unsubscribe();
    }
    if (this.mapTocSubscription) {
      this.mapTocSubscription.unsubscribe();
    }
    if (this.topicId !== undefined) {
      this.router.navigateByUrl('/search');
    } else {
      this.router.navigateByUrl('/maps');
    }
  }

  scrollToTopic(event: any) {
    const node = event.node;
    if (node !== null) {
      const origin = node.origin;
      this.scrollToTopicId(origin.contentId);
    }
  }

  scrollToTopicId(id: string) {
    const el = this.mapTopicsContent.find(e => e.id === id);
    const index = this.mapTopicsContent.findIndex(e => e.id === id);
    if (index > -1 && this.viewPort !== undefined) {
      if (el !== undefined) {
        const element = document.getElementById(el.id);
        if (element !== undefined && element?.offsetTop !== undefined) {
          this.viewPort.scrollToOffset(element?.offsetTop, 'smooth');
        }
      }
    }
  }
}