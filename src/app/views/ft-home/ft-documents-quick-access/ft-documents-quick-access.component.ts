import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { untilDestroyed } from 'src/helpers/rxjs-utils';
import { FtDocument } from 'src/models/fluid-topics/common/ft-document';
import { FluidTopicsService } from 'src/services/fluid-topics-api-service';

@Component({
  selector: 'ft-documents-quick-access',
  templateUrl: './ft-documents-quick-access.component.html',
  styleUrls: ['./ft-documents-quick-access.component.scss']
})
export class FtDocumentsQuickAccessComponent implements OnInit, OnDestroy {

  documents: FtDocument[] = [];

  constructor(
    private fluidTopicsService: FluidTopicsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.fluidTopicsService.documents$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(documents => {
      this.documents = documents;
    });

    this.fluidTopicsService.fetchDocuments();
  }

  ngOnDestroy(): void { }

  openDocument(document: FtDocument) {
    const openMode = document.metadata.find(m => m.key === 'ft:openMode');
    if (openMode?.values && openMode?.values.length > 0) {
      if (openMode?.values[0] === 'external' && document.mimeType === 'text/html') {
        this.fluidTopicsService.getDocument(document.documentApiEndpoint).subscribe(doc => {
          if (doc.prettyUrl) {
            window.open('https://doc.fluidtopics.com' + doc.prettyUrl);
          } else if (doc.viewerUrl) {
            window.open('https://doc.fluidtopics.com' + doc.viewerUrl);
          }
        });
      } else if (openMode?.values[0] === 'fluidtopics' && document.mimeType === 'application/pdf') {
        this.router.navigateByUrl('/pdf/' + document.id);
      }
    }
  }
  
}