import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ft-home',
  templateUrl: './ft-home.component.html',
  styleUrls: ['./ft-home.component.scss']
})
export class FtHomeComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void { }

  ngOnDestroy(): void { }

  redirectToSearch() {
    this.router.navigateByUrl('/search');
  }
}