import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { untilDestroyed } from 'src/helpers/rxjs-utils';
import { FluidTopicsService } from 'src/services/fluid-topics-api-service';

@Component({
  selector: 'ft-pdf-viewer',
  templateUrl: './ft-pdf-viewer.component.html',
  styleUrls: ['./ft-pdf-viewer.component.scss']
})
export class FtPdfViewerComponent implements OnInit, OnDestroy {

  pdfSrc: string = '';

  routeParams$ = this.route.paramMap
  .pipe(
    map(paramMap => 
      paramMap.get('documentId')
    )
  );

  constructor(
    private fluidTopicsService: FluidTopicsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.routeParams$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(documentId => {

      if (documentId !== null) {
        this.pdfSrc = 'https://doc.antidot.net/api/khub/documents/' + documentId + '/content';
      }
    });
  }

  ngOnDestroy(): void { }

}
