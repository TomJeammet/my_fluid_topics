import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { untilDestroyed } from 'src/helpers/rxjs-utils';
import { FtDocument } from 'src/models/fluid-topics/common/ft-document';
import { FtMetadata } from 'src/models/fluid-topics/common/ft-metadata';
import { FtRearrangedSearchResult, FtRearrangedSearchResultElement } from 'src/models/fluid-topics/custom/rearranged-search-results';
import { FtSearchResult } from 'src/models/fluid-topics/ft-search-results/ft-search-result';
import { FtSearchResultElementEntryType } from 'src/models/fluid-topics/ft-search-results/ft-search-result-element-entry-type';
import { FtSuggestionsResult } from 'src/models/fluid-topics/ft-suggestions/ft-suggestions-result';
import { FluidTopicsService } from 'src/services/fluid-topics-api-service';

@Component({
  selector: 'ft-search-results',
  templateUrl: './ft-search-results.component.html',
  styleUrls: ['./ft-search-results.component.scss']
})
export class FtSearchResultsComponent implements OnInit, OnDestroy {

  @Output() newPageSize = new EventEmitter<number>();
  @Output() newPageIndex = new EventEmitter<number>();

  suggestionResult?: FtSuggestionsResult;
  rearrangedSearchResult?: FtRearrangedSearchResult;

  constructor(
    private fluidTopicsService: FluidTopicsService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.fluidTopicsService.searchResult$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(searchResult => {
      if (searchResult) {
        this.parseSearchResult(searchResult);
      }
    });
  }

  ngOnDestroy(): void { }

  parseSearchResult(searchResult: FtSearchResult) {

    let rearrangedSearchResult: FtRearrangedSearchResult = {
      facets: [ ...searchResult.facets ],
      paging: searchResult.paging,
      results: []
    };

    for (const result of searchResult.results) {
      let rearrangedSearchResultElement: FtRearrangedSearchResultElement = {
        entries: [],
        metadataVariableAxis: result.metadataVariableAxis === 'FT_Version' ? 'Version_FT' : result.metadataVariableAxis
      };
      if (result.entries !== null && result.entries.length > 0) {
        rearrangedSearchResultElement.type = result.entries[0].type;
        const first = result.entries[0];
        let firstMetadatas: FtMetadata[] = [];
        if (rearrangedSearchResultElement.type === FtSearchResultElementEntryType.MAP && first.map && first.map.metadata) {
          rearrangedSearchResultElement.title = first.map.title;
          firstMetadatas = first.map.metadata;
        } else if (rearrangedSearchResultElement.type === FtSearchResultElementEntryType.TOPIC && first.topic && first.topic.metadata) {
          rearrangedSearchResultElement.title = first.topic.title;
          firstMetadatas = first.topic.metadata;
        } else if (rearrangedSearchResultElement.type === FtSearchResultElementEntryType.DOCUMENT && first.document && first.document.metadata) {
          rearrangedSearchResultElement.title = first.document.title;
          firstMetadatas = first.document.metadata;
        }
        const foundMetadataVariableAxis = firstMetadatas.find(m => m.key === rearrangedSearchResultElement.metadataVariableAxis);
        if (foundMetadataVariableAxis) {
          rearrangedSearchResultElement.metadataVariableAxisLabel = foundMetadataVariableAxis.label;
        }
      }
      for (const entry of result.entries) {
        if (entry.type === FtSearchResultElementEntryType.MAP && entry.map) {
          rearrangedSearchResultElement.entries.push(entry.map);
        } else if (entry.type === FtSearchResultElementEntryType.TOPIC && entry.topic) {
          rearrangedSearchResultElement.entries.push(entry.topic);
        } else if (entry.type === FtSearchResultElementEntryType.DOCUMENT && entry.document) {
          rearrangedSearchResultElement.entries.push(entry.document);
        }
      }
      rearrangedSearchResult.results.push(rearrangedSearchResultElement);
    }

    this.rearrangedSearchResult = rearrangedSearchResult;
    // console.log('rearrangedSearchResult : ', rearrangedSearchResult);
  }

  changePageSize(newPageSize: number) {
    this.newPageSize.emit(newPageSize);
  }

  changePageIndex(newPageIndex: number) {
    this.newPageIndex.emit(newPageIndex);
  }

  getContent(result: FtRearrangedSearchResultElement) {

    console.log('result selected : ', result);
    switch (result.type) {
      case FtSearchResultElementEntryType.MAP:
        let selectedMap: any;
        if (result.selectedIndex !== undefined) {
          selectedMap = result.entries[result.selectedIndex];
        } else {
          selectedMap = result.entries[0];
        }
        this.router.navigateByUrl('/maps/' + selectedMap.mapId);
        break;
      case FtSearchResultElementEntryType.TOPIC:
        let selectedTopic: any;
        if (result.selectedIndex !== undefined) {
          selectedTopic = result.entries[result.selectedIndex];
        } else {
          selectedTopic = result.entries[0];
        }
        this.router.navigateByUrl('/maps/' + selectedTopic.mapId + '/topic/' + selectedTopic.contentId);
        break;
      case FtSearchResultElementEntryType.DOCUMENT:
        let selectedDocument: any;
        if (result.selectedIndex !== undefined) {
          selectedDocument = result.entries[result.selectedIndex];
        } else {
          selectedDocument = result.entries[0];
        }
        this.openDocument(selectedDocument);
        break;
      default:
        break;
    }
  }

  openDocument(document: FtDocument) {
    const openMode = document.metadata.find(m => m.key === 'ft:openMode');
    if (openMode?.values && openMode?.values.length > 0) {
      if (openMode?.values[0] === 'external' && document.mimeType === 'text/html') {
        this.fluidTopicsService.getDocument(document.documentUrl).subscribe(doc => {
          if (doc.prettyUrl) {
            window.open('https://doc.fluidtopics.com' + doc.prettyUrl);
          } else if (doc.viewerUrl) {
            window.open('https://doc.fluidtopics.com' + doc.viewerUrl);
          }
        });
      } else if (openMode?.values[0] === 'fluidtopics' && document.mimeType === 'application/pdf') {
        this.router.navigateByUrl('/pdf/' + document.documentId);
      }
    }
  }
  
}