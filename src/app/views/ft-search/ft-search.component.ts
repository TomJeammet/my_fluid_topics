import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { firstValueOfControl, untilDestroyed } from 'src/helpers/rxjs-utils';
import { defaultLocale, FtLocale } from 'src/models/fluid-topics/common/ft-locale';
import { FtExtendedSearchFilters } from 'src/models/fluid-topics/custom/extended-search-filters';
import { defaultSearchFilterPaging, FtSearchFilterPaging } from 'src/models/fluid-topics/ft-search-filters/ft-search-filter-paging';
import { FtSearchFilters } from 'src/models/fluid-topics/ft-search-filters/ft-search-filters';
import { FtSuggestion } from 'src/models/fluid-topics/ft-suggestions/ft-suggestion';
import { FtSuggestionsResult } from 'src/models/fluid-topics/ft-suggestions/ft-suggestions-result';
import { FluidTopicsService } from 'src/services/fluid-topics-api-service';

@Component({
  selector: 'ft-search',
  templateUrl: './ft-search.component.html',
  styleUrls: ['./ft-search.component.scss']
})
export class FtSearchComponent implements OnInit, OnDestroy {

  searchForm: FormGroup;

  suggestionResult?: FtSuggestionsResult;
  suggestions: FtSuggestion[] = [];
  suggestionEnabled: boolean = true;

  currentLocale: FtLocale = defaultLocale;
  defaultSearch: FtSearchFilters = {
    query: '',
    contentLocale: defaultLocale.lang,
    filters: [],
    sort: [],
    facets: [],
    paging: defaultSearchFilterPaging
  };

  constructor(
    private fluidTopicsService: FluidTopicsService,
    private formBuilder: FormBuilder
  ) {
    this.searchForm = this.formBuilder.group(this.defaultSearch);
  }

  ngOnInit() {

    firstValueOfControl(this.searchForm).pipe(debounceTime(500), distinctUntilChanged())
    .pipe(
      untilDestroyed(this)
    )
    .subscribe(_ => {
      this.suggestionEnabled = false;
      let filters : FtSearchFilters = this.searchForm.value;
        this.fluidTopicsService.fetchSearchResult(filters);
      if (this.searchForm.controls['query'].value) {
        this.fluidTopicsService.fetchSuggest(this.searchForm.controls['query'].value);
      } else {
        this.suggestions = [];
      }
    });

    this.fluidTopicsService.suggestionResult$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(suggestionResult => {
      if (!this.searchForm.controls['query'].pristine) {
        this.suggestionEnabled = true;
        this.searchForm.controls['query'].markAsPristine();
      }
      this.suggestionResult = suggestionResult;
      if (suggestionResult && suggestionResult.suggestions && suggestionResult.suggestions.length > 0) {
        this.suggestions = [...suggestionResult.suggestions];
        if (suggestionResult.suggestions.length === 1 && suggestionResult.suggestions[0].value === this.searchForm.controls['query'].value) {
          this.suggestionEnabled = false;
        }
      } else {
        this.suggestions = [];
      }
    });
  }

  ngOnDestroy() { }

  setQuery(selectedQuery: string) {
    this.searchForm.controls['query'].setValue(selectedQuery);
  }

  disableSuggestions() {
    if (this.suggestionEnabled) {
      this.suggestionEnabled = false;
    }
  }

  updateLocaleInForm(newLocale: FtLocale) {
    this.searchForm.controls['contentLocale'].setValue(newLocale.lang);
  }

  updateFormWithFilters(extendedSearchFilters: FtExtendedSearchFilters) {
    this.searchForm.controls['filters'].setValue(extendedSearchFilters.filters);
    this.searchForm.controls['contentLocale'].setValue(extendedSearchFilters.locale.lang);
  }

  changePageSizeInForm(newPageSize: any) {
    let currentPaging: FtSearchFilterPaging = this.searchForm.controls['paging'].value;
    currentPaging.perPage = newPageSize;
    this.searchForm.controls['paging'].setValue(currentPaging);
  }

  changePageIndexInForm(newPageIndex: any) {
    let currentPaging: FtSearchFilterPaging = this.searchForm.controls['paging'].value;
    currentPaging.page = newPageIndex;
    this.searchForm.controls['paging'].setValue(currentPaging);
  }
}