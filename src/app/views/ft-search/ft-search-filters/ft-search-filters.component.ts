import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { untilDestroyed } from 'src/helpers/rxjs-utils';
import { FtFilter } from 'src/models/fluid-topics/common/ft-filter';
import { defaultLocale, FtLocale } from 'src/models/fluid-topics/common/ft-locale';
import { FtExtendedSearchFilters } from 'src/models/fluid-topics/custom/extended-search-filters';
import { FluidTopicsService } from 'src/services/fluid-topics-api-service';

@Component({
  selector: 'ft-search-filters',
  templateUrl: './ft-search-filters.component.html',
  styleUrls: ['./ft-search-filters.component.scss']
})
export class FtSearchFiltersComponent implements OnInit, OnDestroy {

  @Output() extendedFilters = new EventEmitter<FtExtendedSearchFilters>();

  // n'ayant pas trouvé d'API retournant exclusivement la liste des filtres,
  // les filtres suivants sont renseignés en durs pour être accessibles lors de la recherche
  versionFilters: FtFilter = {
    key: "Version_FT",
    values: ["Latest", "3.10", "3.9", "3.8", "3.7", "3.6", "3.5", "3.4", "3.3", "3.2", "3.1"]
  };
  selectedVersionFilters: FtFilter = { key: 'Version_FT', values: [] };
  
  categoryFilters: FtFilter = {
    key: "Category",
    values: ["Reference Guides", "Release Notes", "Screencasts", "Technical Notes", "Tips"]
  };
  selectedCategoryFilters: FtFilter[] = [];

  osFilters: FtFilter = {
    key: "Platform",
    values: ["Debian", "RHEL"]
  };
  selectedOsFilters: FtFilter[] = [];

  audienceFilters: FtFilter = {
    key: "audience",
    values: ["public"]
  };
  selectedAudienceFilters: FtFilter[] = [];

  activeFilters: FtFilter[] = [];

  locales: FtLocale[] = [];
  currentLocale: FtLocale = defaultLocale;

  constructor(
    private fluidTopicsService: FluidTopicsService
  ) { }

  ngOnInit(): void {
    this.fluidTopicsService.locales$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(locales => {
      this.locales = locales;
    });

    this.fluidTopicsService.fetchLocales();
  }

  ngOnDestroy(): void { }

  updateSelectedFilters(key: string, values: string[]) {
    let currentActiveFilters: FtFilter[] = [ ...this.activeFilters ];
    let matchingKeyFilter: FtFilter | undefined = currentActiveFilters.find(f => f.key === key);
    if (values.length > 0) {
      if (matchingKeyFilter) {
        matchingKeyFilter.values = [ ...values ];
      } else {
        let newMatchingKeyFilter: FtFilter = {
          key,
          values
        };
        currentActiveFilters.push(newMatchingKeyFilter);
      }
      this.activeFilters = [ ...currentActiveFilters ];
      this.emitExtendedFilters();
    } else {
      if (matchingKeyFilter) {
        const matchingKeyFilterIndex = currentActiveFilters.findIndex(f => f.key === key);
        currentActiveFilters = [
          ...currentActiveFilters.slice(0, matchingKeyFilterIndex),
          ...currentActiveFilters.slice(matchingKeyFilterIndex + 1)
        ];
        this.activeFilters = [ ...currentActiveFilters ];
        this.emitExtendedFilters();
      }
    }
  }

  emitExtendedFilters() {
    const newExtendedSearchFilters: FtExtendedSearchFilters = {
      locale: this.currentLocale,
      filters: this.activeFilters
    };
    this.extendedFilters.emit(newExtendedSearchFilters);
  }
  
}