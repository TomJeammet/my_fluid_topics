import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FtHomeComponent } from './views/ft-home/ft-home.component';
import { FtSearchComponent } from './views/ft-search/ft-search.component';
import { FtMapExplorerComponent } from './views/ft-map-explorer/ft-map-explorer.component';
import { FtPdfViewerComponent } from './views/ft-pdf-viewer/ft-pdf-viewer.component';

const routes: Routes = [
  {
    path: '',
    component: FtHomeComponent
  },
  {
    path: 'search',
    component: FtSearchComponent
  },
  {
    path: 'maps/:mapId/topic/:topicId',
    component: FtMapExplorerComponent
  },
  {
    path: 'maps/:mapId',
    component: FtMapExplorerComponent
  },
  {
    path: 'maps',
    component: FtMapExplorerComponent
  },
  {
    path: 'pdf/:documentId',
    component: FtPdfViewerComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
