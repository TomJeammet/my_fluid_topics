import { CdkAccordionModule } from '@angular/cdk/accordion';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import fr from '@angular/common/locales/fr';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { fr_FR, NZ_I18N } from 'ng-zorro-antd/i18n';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FtHeaderComponent } from './ft-header/ft-header.component';
import { FtDocumentsQuickAccessComponent } from './views/ft-home/ft-documents-quick-access/ft-documents-quick-access.component';
import { FtHomeComponent } from './views/ft-home/ft-home.component';
import { FtSearchFiltersComponent } from './views/ft-search/ft-search-filters/ft-search-filters.component';
import { FtSearchResultsComponent } from './views/ft-search/ft-search-results/ft-search-results.component';
import { FtSearchComponent } from './views/ft-search/ft-search.component';
import { FtSidebarComponent } from './ft-sidebar/ft-sidebar.component';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { FtMapExplorerComponent } from './views/ft-map-explorer/ft-map-explorer.component';
import { FtPdfViewerComponent } from './views/ft-pdf-viewer/ft-pdf-viewer.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

registerLocaleData(fr);

@NgModule({
  declarations: [
    AppComponent,
    FtHomeComponent,
    FtHeaderComponent,
    FtSearchFiltersComponent,
    FtSearchResultsComponent,
    FtDocumentsQuickAccessComponent,
    FtSearchComponent,
    FtSidebarComponent,
    FtMapExplorerComponent,
    FtPdfViewerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NzInputModule,
    FormsModule,
    ReactiveFormsModule,
    NzListModule,
    ScrollingModule,
    NzCollapseModule,
    CdkAccordionModule,
    NzTagModule,
    NzSelectModule,
    BrowserAnimationsModule,
    NzPaginationModule,
    NzButtonModule,
    NzIconModule,
    NzTreeModule,
    PdfViewerModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: fr_FR }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);