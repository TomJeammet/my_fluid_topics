import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'ft-sidebar',
  templateUrl: './ft-sidebar.component.html',
  styleUrls: ['./ft-sidebar.component.scss']
})
export class FtSidebarComponent implements OnInit, OnDestroy {

  currentPage?: 'home' | 'search' | 'maps';

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url !== null) {
          const segments = event.url.split('/');
          if (segments !== null) {
            let basePath: string = '';
            if (segments.length > 1) {
              basePath = segments[1];
            } else if (segments.length === 0) {
              basePath = segments[0];
            }
            if (basePath === '') {
              this.currentPage = 'home';
            } else if (basePath === 'search') {
              this.currentPage = 'search';
            } else if (basePath === 'maps') {
              this.currentPage = 'maps';
            } else {
              this.currentPage = undefined;
            }
          }
        }
      }
    });
  }

  ngOnDestroy(): void { }

  redirectTo(path: string) {
    this.router.navigateByUrl(path);
  }
}