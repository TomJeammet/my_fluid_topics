import { Component, OnDestroy, OnInit} from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: 'ft-header',
  templateUrl: './ft-header.component.html',
  styleUrls: ['./ft-header.component.scss']
})
export class FtHeaderComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void { }

  ngOnDestroy(): void { }

  redirectToHome() {
    this.router.navigateByUrl('/');
  }
}