import { AbstractControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil, startWith } from 'rxjs/operators';

export function firstValueOfControl<T>(control: AbstractControl) {
  return control.valueChanges.pipe(startWith(control.value));
}

function isFunction(value: any) {
  return typeof value === 'function';
}

export function untilDestroyed<T>(componentInstance: any, destroyMethodName = 'ngOnDestroy') {

  return <U>(source: Observable<T>) => {
    const originalDestroy = componentInstance[destroyMethodName];
    if (isFunction(originalDestroy) === false) {
      throw new Error(
        `${
        componentInstance.constructor.name
        } is using untilDestroyed but doesn't implement ${destroyMethodName}`
      );
    }
    if (!componentInstance.__takeUntilDestroy) {
      componentInstance.__takeUntilDestroy = new Subject();

      componentInstance[destroyMethodName] = function() {
        isFunction(originalDestroy) && originalDestroy.apply(this, arguments);
        componentInstance.__takeUntilDestroy.next(true);
        componentInstance.__takeUntilDestroy.complete();
      };
    }
    return source.pipe(takeUntil<T>(componentInstance.__takeUntilDestroy));
  };
}