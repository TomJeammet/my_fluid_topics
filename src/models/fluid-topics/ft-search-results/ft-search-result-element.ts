import { FtSearchResultElementEntry } from "./ft-search-result-element-entry";

export interface SearchResultElement {
  entries: FtSearchResultElementEntry[];
  metadataVariableAxis?: string;
}