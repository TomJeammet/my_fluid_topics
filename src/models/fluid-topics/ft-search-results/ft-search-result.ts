import { FtSearchResultFacet } from "./ft-search-result-facet/ft-search-result-facet";
import { FtSearchResultPaging } from "./ft-search-result-paging";
import { SearchResultElement } from "./ft-search-result-element";

export interface FtSearchResult {
  facets: FtSearchResultFacet[];
  paging?: FtSearchResultPaging;
  results: SearchResultElement[];
}

export const defaultSearchResult: FtSearchResult = {
  facets: [],
  paging: undefined,
  results: []
}