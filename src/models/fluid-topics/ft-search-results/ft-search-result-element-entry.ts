import { FtDocument } from "../common/ft-document";
import { FtMap } from "../common/map/ft-map";
import { FtTopic } from "../common/topic/ft-topic";
import { FtSearchResultElementEntryType } from "./ft-search-result-element-entry-type";

export interface FtSearchResultElementEntry {
  type?: FtSearchResultElementEntryType;
  topic?: FtTopic;
  map?: FtMap;
  document?: FtDocument;
}