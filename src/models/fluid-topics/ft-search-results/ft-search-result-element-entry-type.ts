export enum FtSearchResultElementEntryType {
  MAP = 'MAP',
  TOPIC = 'TOPIC',
  DOCUMENT = 'DOCUMENT'
}