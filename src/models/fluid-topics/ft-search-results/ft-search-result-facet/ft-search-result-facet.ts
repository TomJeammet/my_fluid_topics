import { FtRootNode } from "./ft-root-node";

export interface FtSearchResultFacet {
  key?: string;
  label?: string;
  hierarchical?: boolean;
  multiSelectionable?: boolean;
  rootNodes: FtRootNode[];
}