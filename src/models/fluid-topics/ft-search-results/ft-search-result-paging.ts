export interface FtSearchResultPaging {
  currentPage?: number;
  totalResultsCount?: number;
  totalCluestersCount?: number;
  isLastPage?: boolean;
}