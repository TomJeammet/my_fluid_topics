export interface FtSorting {
  key: string; // facet id
  order: FtSortingOrder;
}

export enum FtSortingOrder {
  DESC = 'DESC',
  ASC = 'ASC'
}