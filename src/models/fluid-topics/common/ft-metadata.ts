export interface FtMetadata {
  key?: string;
  label?: string;
  values: string[];
}