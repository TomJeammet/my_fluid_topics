export interface FtLocale {
  lang: string;
  label: string;
  count?: number;
}

export const defaultLocale: FtLocale = {
  lang: 'en-US',
  label: 'English'
}