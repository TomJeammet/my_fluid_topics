import { FtLocale } from "./ft-locale";

export interface FtContentLocales {
  contentLocales: FtLocale[];
}