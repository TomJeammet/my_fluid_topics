import { FtMetadata } from "./ft-metadata";

export interface FtDocument {
  id: string;
  filename?: string;
  title?: string;
  mimeType?: string;
  lang?: string;
  lastEdition?: string;
  lastPublication?: string;
  baseId?: string;
  originId?: string;
  clusterId?: string;
  originUrl?: string;
  description?: string;
  prettyUrl: string;
  khubVersion?: string;
  openMode?: string;
  rightsApiEndpoint?: string;
  contentApiEndpoint?: string;
  documentApiEndpoint: string;
  documentUrl: string;
  viewerUrl: string;
  metadata: FtMetadata[];
  documentId: string;
}