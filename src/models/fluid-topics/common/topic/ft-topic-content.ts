export interface FtTopicContent {
  id: string;
  title: string;
  content: string;
}