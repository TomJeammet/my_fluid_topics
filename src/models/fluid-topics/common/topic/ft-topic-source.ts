export interface FtTopicSource {
  id: string;
  name: string;
  type: string;
  description: string;
}