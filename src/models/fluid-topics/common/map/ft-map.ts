import { FtMetadata } from "../ft-metadata";


export interface FtMap {
  title?: string;
  lang?: string;
  id: string;
  mapId?: string;
  originId?: string;
  baseId?: string;
  lastEdition?: string;
  lastPublication?: string;
  clusterId?: string;
  editorialType?: string;
  khubVersion?: string;
  openMode?: string;
  prettyUrl?: string;
  readerUrl?: string;
  rightsApiEndpoint?: string;
  topicsApiEndpoint?: string;
  attachmentsApiEndpoint?: string;
  metadata?: FtMetadata[];
}