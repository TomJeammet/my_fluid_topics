export interface FtMapTableOfContentElement {
  tocId: string;
  contentId: string;
  title: string;
  children: FtMapTableOfContentElement[];
}