export interface FtSearchFilterPaging {
  page: number;
  perPage: number;
}

export const defaultSearchFilterPaging: FtSearchFilterPaging = {
  page: 1,
  perPage: 10
}