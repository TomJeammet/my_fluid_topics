import { FtFilter } from "../common/ft-filter";
import { FtSearchFilterFacet } from "./ft-search-filter-facet";
import { FtSearchFilterPaging } from "./ft-search-filter-paging";
import { FtSorting } from "../common/ft-sorting";

export interface FtSearchFilters {
  query: string;
  contentLocale: string;
  filters: FtFilter[];
  sort: FtSorting[];
  facets: FtSearchFilterFacet[];
  paging: FtSearchFilterPaging;
}