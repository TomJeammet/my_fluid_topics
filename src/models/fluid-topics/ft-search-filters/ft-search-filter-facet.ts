export interface FtSearchFilterFacet {
  id: string;
  maxDepth: number;
}