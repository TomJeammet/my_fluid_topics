export enum FtSuggestionEditorialType {
  BOOK = 'BOOK',
  ARTICLE = 'ARTICLE'
}