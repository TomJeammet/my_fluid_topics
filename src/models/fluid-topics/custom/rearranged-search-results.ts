import { FtDocument } from "../common/ft-document";
import { FtMap } from "../common/map/ft-map";
import { FtTopic } from "../common/topic/ft-topic";
import { FtSearchResultElementEntryType } from "../ft-search-results/ft-search-result-element-entry-type";
import { FtSearchResultFacet } from "../ft-search-results/ft-search-result-facet/ft-search-result-facet";
import { FtSearchResultPaging } from "../ft-search-results/ft-search-result-paging";

export interface FtRearrangedSearchResult {
  facets: FtSearchResultFacet[];
  paging?: FtSearchResultPaging;
  results: FtRearrangedSearchResultElement[];
}

export interface FtRearrangedSearchResultElement {
  entries: (FtTopic | FtMap | FtDocument)[];
  metadataVariableAxis?: string;
  metadataVariableAxisLabel?: string;
  type?: FtSearchResultElementEntryType; // changed so the type is here as he is common in all child entries
  title?: string;
  selectedIndex?: number;
}