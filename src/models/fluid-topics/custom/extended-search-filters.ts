import { FtFilter } from "../common/ft-filter";
import { FtLocale } from "../common/ft-locale";

export interface FtExtendedSearchFilters {
  locale: FtLocale;
  filters: FtFilter[];
}